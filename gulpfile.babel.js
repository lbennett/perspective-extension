import gulp from 'gulp';
import sass from 'gulp-ruby-sass';
import imagemin from 'gulp-imagemin';
import browserify from 'browserify';
import sourcemaps from 'gulp-sourcemaps';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import watchify from 'watchify';
import babelify from 'babelify';

const Bundler = {
  initBundler() {
    const options = { debug: true };
    const transform = browserify('./src/content/index.js', options).transform(babelify);

    this.bundler = watchify(transform);
  },

  compile() {
    if (!this.bundler) this.initBundler();

    this.rebundle();
  },

  watch() {
    if (!this.bundler) this.initBundler();

    this.bindWatch();
    this.compile();
  },

  rebundle() {
    this.bundler.bundle()
      .on('error', this.bundleError)
      .pipe(source('content.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./dist'));
  },

  bindWatch() {
    this.bundler.on('update', this.rebundle.bind(this));
  },

  bundleError(error) {
    /* eslint-disable no-console */
    console.error(error);
    /* eslint-enable no-console */
    this.emit('end');
  },
};

gulp.task('js', () => Bundler.compile());

gulp.task('js:dev', ['js'], () => Bundler.watch());

gulp.task('scss', () => sass('src/style.scss').pipe(gulp.dest('dist')));

gulp.task('scss:dev', ['scss'], () => gulp.watch('src/style.scss', ['scss']));

gulp.task('icons', () => {
  gulp.src('src/icons/*.png')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/icons'));
});

gulp.task('icons:dev', ['icons'], () => gulp.watch('src/icons/*.png', ['icons']));

gulp.task('manifest', () => {
  gulp.src('src/manifest.json')
    .pipe(gulp.dest('dist'));
});

gulp.task('manifest:dev', ['manifest'], () => gulp.watch('src/manifest.json', ['manifest']));

gulp.task('config', () => {
  gulp.src('src/config.json')
    .pipe(gulp.dest('dist'));
});

gulp.task('config:dev', ['config'], () => gulp.watch('src/config.json', ['config']));

gulp.task('build', ['js', 'scss', 'icons', 'manifest', 'config']);

gulp.task('dev', ['js:dev', 'scss:dev', 'icons:dev', 'manifest:dev', 'config:dev']);

gulp.task('default', ['build']);
