(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports={
  "KEY": "AIzaSyD61ZcGPH_phZ_dV0z1v6gfINdhNUaZC5o"
}

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LIGHT_BLUE = [79, 195, 247];
var DARK_BLUE = [41, 121, 255];
var PURPLE = [98, 0, 234];
var RED = [239, 46, 46];

var RGB_STRING_VALUES_REGEX = /\d{1,3}/g;

var RGB_VALUE_INCREMENT = 3;

var Color = function () {
  function Color(element) {
    _classCallCheck(this, Color);

    this.element = element;
  }

  _createClass(Color, [{
    key: 'setDefaultColor',
    value: function setDefaultColor() {
      this.setRGB(DARK_BLUE);
    }
  }, {
    key: 'setColorFromValue',
    value: function setColorFromValue(value) {
      if (value <= 0.4) return this.animateLightBlue();
      if (value >= 0.6) return this.animatePurple();
      return this.animateDarkBlue();
    }
  }, {
    key: 'animateDefaultColor',
    value: function animateDefaultColor() {
      this.animateDarkBlue();
    }
  }, {
    key: 'animateLightBlue',
    value: function animateLightBlue() {
      this.animateColor(LIGHT_BLUE);
    }
  }, {
    key: 'animateDarkBlue',
    value: function animateDarkBlue() {
      this.animateColor(DARK_BLUE);
    }
  }, {
    key: 'animatePurple',
    value: function animatePurple() {
      this.animateColor(PURPLE);
    }
  }, {
    key: 'animateRed',
    value: function animateRed() {
      this.animateColor(RED);
    }
  }, {
    key: 'animateColor',
    value: function animateColor(rgb) {
      window.requestAnimationFrame(this.animationLoop.bind(this, rgb));
    }
  }, {
    key: 'animationLoop',
    value: function animationLoop(rgb) {
      var currentRGB = this.getRGB();

      console.log('stop?', Color.isCurrentRGB(rgb, currentRGB));

      if (Color.isCurrentRGB(rgb, currentRGB)) return;

      var nextRGB = Color.getNextRGB(rgb, currentRGB);

      this.setRGB(nextRGB);
      this.animateColor(rgb);
    }
  }, {
    key: 'setRGB',
    value: function setRGB(rgb) {
      this.element.style.backgroundColor = Color.renderRGB(rgb);
    }
  }, {
    key: 'getRGB',
    value: function getRGB() {
      return Color.parseRGB(this.element.style.backgroundColor);
    }
  }], [{
    key: 'renderRGB',
    value: function renderRGB(rgb) {
      return 'rgb(' + rgb.join(',') + ')';
    }
  }, {
    key: 'parseRGB',
    value: function parseRGB(rgbString) {
      var rgbValues = rgbString.match(RGB_STRING_VALUES_REGEX);

      return [parseInt(rgbValues[0], 10), parseInt(rgbValues[1], 10), parseInt(rgbValues[2], 10)];
    }
  }, {
    key: 'isCurrentRGB',
    value: function isCurrentRGB(rgb, otherRGB) {
      return rgb.every(function (value, index) {
        return Color.valuesInBounds(value, otherRGB[index]);
      });
    }
  }, {
    key: 'getNextRGB',
    value: function getNextRGB(rgb, currentRGB) {
      return currentRGB.map(function (value, index) {
        if (value > rgb[index]) return value - RGB_VALUE_INCREMENT;
        if (value < rgb[index]) return value + RGB_VALUE_INCREMENT;
        return value;
      });
    }
  }, {
    key: 'valuesInBounds',
    value: function valuesInBounds(value, otherValue) {
      var min = otherValue - RGB_VALUE_INCREMENT;
      var max = otherValue + RGB_VALUE_INCREMENT;

      return value >= min && value <= max;
    }
  }]);

  return Color;
}();

exports.default = Color;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Color = require('./Color');

var _Color2 = _interopRequireDefault(_Color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CONTAINER_PADDING = 3;

var Container = function () {
  function Container() {
    _classCallCheck(this, Container);

    this.element = document.createElement('div');
    this.element.classList.add('perspective-extension-container');

    this.color = new _Color2.default(this.element);
  }

  _createClass(Container, [{
    key: 'hide',
    value: function hide() {
      this.element.classList.remove('show');
    }
  }, {
    key: 'show',
    value: function show() {
      this.element.classList.add('show');
    }
  }, {
    key: 'position',
    value: function position(inputElement) {
      var position = this.calculatePosition(inputElement);

      this.element.style.top = this.calculateTop(inputElement, position.top) + 'px';
      this.element.style.left = this.calculateLeft(inputElement, position.left) + 'px';
    }
  }, {
    key: 'calculateTop',
    value: function calculateTop(inputElement, positionTop) {
      var top = positionTop;
      top += inputElement.offsetHeight / 2;
      top -= this.element.offsetHeight / 2;

      return Math.round(top);
    }
  }, {
    key: 'calculateLeft',
    value: function calculateLeft(inputElement, positionLeft) {
      var left = positionLeft;
      left += inputElement.offsetWidth;
      left -= this.element.offsetWidth;
      left -= CONTAINER_PADDING;

      return Math.round(left);
    }
  }, {
    key: 'calculatePosition',
    value: function calculatePosition(element) {
      var position = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { top: 0, left: 0 };

      if (element.tagName === 'BODY') return position;

      position.left += element.offsetLeft;
      position.top += element.offsetTop;

      return this.calculatePosition(element.offsetParent, position);
    }
  }]);

  return Container;
}();

exports.default = Container;

},{"./Color":2}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Input = require('./Input');

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TAG_REGEX = /^(input|textarea)$/i;

var Extension = function () {
  function Extension() {
    _classCallCheck(this, Extension);

    this.inputs = [];
  }

  _createClass(Extension, [{
    key: 'init',
    value: function init() {
      this.bindDocumentEvents();
    }
  }, {
    key: 'bindDocumentEvents',
    value: function bindDocumentEvents() {
      document.addEventListener('focusin', this.initInput.bind(this));
      // document.addEventListener('focusout', event => this.hideInputContainer(event.target));
    }
  }, {
    key: 'initInput',
    value: function initInput(event) {
      var target = event.target;

      if (!TAG_REGEX.test(target.tagName)) return;

      if (this.showInput(target)) return;

      var input = new _Input2.default(target);
      this.inputs.push(input);
      input.init();
    }
  }, {
    key: 'showInput',
    value: function showInput(inputElement) {
      var existingInput = this.getInput(inputElement);
      if (existingInput) existingInput.container.show();

      return existingInput;
    }
  }, {
    key: 'hideInputContainer',
    value: function hideInputContainer(inputElement) {
      var existingInput = this.getInput(inputElement);
      if (existingInput) existingInput.container.hide();

      return existingInput;
    }
  }, {
    key: 'getInput',
    value: function getInput(inputElement) {
      return this.inputs.find(function (input) {
        return input.element === inputElement;
      });
    }
  }]);

  return Extension;
}();

exports.default = Extension;

},{"./Input":5}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Container = require('./Container');

var _Container2 = _interopRequireDefault(_Container);

var _Service = require('./Service');

var _Service2 = _interopRequireDefault(_Service);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Input = function () {
  function Input(element) {
    _classCallCheck(this, Input);

    this.element = element;
  }

  _createClass(Input, [{
    key: 'init',
    value: function init() {
      this.createContainer();
      this.element.addEventListener('input', this.requestPerspective.bind(this));
    }
  }, {
    key: 'createContainer',
    value: function createContainer() {
      this.container = new _Container2.default();

      document.body.appendChild(this.container.element);

      this.container.color.setDefaultColor();
      this.container.position(this.element);
      this.container.show();
    }
  }, {
    key: 'requestPerspective',
    value: function requestPerspective(event) {
      var value = event.target.value;

      _Service2.default.debounceRequest(value, this.renderPerspective.bind(this));
    }
  }, {
    key: 'renderPerspective',
    value: function renderPerspective(error, data) {
      if (error) return this.renderError(error);

      var value = data.attributeScores.TOXICITY.summaryScore.value;
      return this.container.color.setColorFromValue(value);
    }
  }, {
    key: 'renderError',
    value: function renderError(error) {
      if (error.message === 'No text value provided.') return this.container.color.animateDefaultColor();

      this.container.color.animateRed();
      /* eslint-disable no-console */
      console.error('error', error);
      /* eslint-enable no-console */
    }
  }]);

  return Input;
}();

exports.default = Input;

},{"./Container":3,"./Service":6}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ENDPOINT_URL = 'https://commentanalyzer.googleapis.com/v1alpha1/comments:analyze';
var DEBOUNCE_DURATION = 500;

var Service = {
  ENDPOINT_URL: ENDPOINT_URL,
  DEBOUNCE_DURATION: DEBOUNCE_DURATION,

  request: function request(value, callback) {
    var _this = this;

    if (!value) return callback(new Error('No text value provided.'));

    var data = this.buildPayload(value);
    var endpoint = this.buildEndpoint();
    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function (event) {
      return _this.requestStateChange(event, callback);
    });
    xhr.open('POST', endpoint);

    xhr.send(data);

    return xhr;
  },
  requestStateChange: function requestStateChange(event, callback) {
    var target = event.target;

    if (target.readyState !== XMLHttpRequest.DONE) return;

    var data = JSON.parse(target.responseText);
    var callbackArguments = [data];
    if (target.status === 200) callbackArguments.unshift(null);

    callback.apply(undefined, callbackArguments);
  },
  buildPayload: function buildPayload(text) {
    var data = {
      comment: {
        text: text
      },
      requestedAttributes: {
        TOXICITY: {}
      }
    };

    return JSON.stringify(data);
  },
  buildEndpoint: function buildEndpoint() {
    return this.ENDPOINT_URL + '?key=' + _config2.default.KEY;
  },
  debounceRequest: function debounceRequest(value, callback) {
    if (this.timeout) clearTimeout(this.timeout);

    this.timeout = setTimeout(this.request.bind(this, value, callback), this.DEBOUNCE_DURATION);
  }
};

exports.default = Service;

},{"../config.json":1}],7:[function(require,module,exports){
'use strict';

var _Extension = require('./Extension');

var _Extension2 = _interopRequireDefault(_Extension);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var extension = new _Extension2.default();
extension.init();

},{"./Extension":4}]},{},[7])

//# sourceMappingURL=content.js.map
