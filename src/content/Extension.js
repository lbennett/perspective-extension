import Input from './Input';

const TAG_REGEX = /^(input|textarea)$/i;

class Extension {
  constructor() {
    this.inputs = [];
  }

  init() {
    this.bindDocumentEvents();
  }

  bindDocumentEvents() {
    document.addEventListener('focusin', this.initInput.bind(this));
    document.addEventListener('focusout', event => this.resetContainer(event.target));
  }

  initInput(event) {
    const target = event.target;

    if (!TAG_REGEX.test(target.tagName)) return;

    if (this.showInput(target)) return;

    const input = new Input(target);
    this.inputs.push(input);
    input.init();
  }

  showInput(inputElement) {
    const existingInput = this.getInput(inputElement);
    if (existingInput) existingInput.container.show();

    return existingInput;
  }

  resetContainer(inputElement) {
    const existingInput = this.getInput(inputElement);
    if (existingInput) existingInput.container.color.animateDefaultColor();

    return existingInput;
  }

  getInput(inputElement) {
    return this.inputs.find(input => input.element === inputElement);
  }
}

export default Extension;
