import CONFIG from '../config.json';

const ENDPOINT_URL = 'https://commentanalyzer.googleapis.com/v1alpha1/comments:analyze';
const DEBOUNCE_DURATION = 500;

const Service = {
  ENDPOINT_URL,
  DEBOUNCE_DURATION,

  request(value, callback) {
    if (!value) return callback(new Error('No text value provided.'));

    const data = this.buildPayload(value);
    const endpoint = this.buildEndpoint();
    const xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', event => this.requestStateChange(event, callback));
    xhr.open('POST', endpoint);

    xhr.send(data);

    return xhr;
  },

  requestStateChange(event, callback) {
    const target = event.target;

    if (target.readyState !== XMLHttpRequest.DONE) return;

    const data = JSON.parse(target.responseText);
    const callbackArguments = [data];
    if (target.status === 200) callbackArguments.unshift(null);

    callback(...callbackArguments);
  },

  buildPayload(text) {
    const data = {
      comment: {
        text,
      },
      requestedAttributes: {
        TOXICITY: {},
      },
    };

    return JSON.stringify(data);
  },

  buildEndpoint() {
    return `${this.ENDPOINT_URL}?key=${CONFIG.KEY}`;
  },

  debounceRequest(value, callback) {
    if (this.timeout) clearTimeout(this.timeout);

    this.timeout = setTimeout(this.request.bind(this, value, callback), this.DEBOUNCE_DURATION);
  },
};

export default Service;
