const LIGHT_BLUE = [79, 195, 247];
const DARK_BLUE = [41, 121, 255];
const PURPLE = [98, 0, 234];
const RED = [239, 46, 46];

const RGB_STRING_VALUES_REGEX = /\d{1,3}/g;

const RGB_VALUE_INCREMENT = 3;

class Color {
  constructor(element) {
    this.element = element;
  }

  setDefaultColor() {
    this.setRGB(DARK_BLUE);
  }

  setColorFromValue(value) {
    if (value <= 0.4) return this.animateLightBlue();
    if (value >= 0.6) return this.animatePurple();
    return this.animateDarkBlue();
  }

  animateDefaultColor() {
    this.animateDarkBlue();
  }

  animateLightBlue() {
    this.animateColor(LIGHT_BLUE);
  }

  animateDarkBlue() {
    this.animateColor(DARK_BLUE);
  }

  animatePurple() {
    this.animateColor(PURPLE);
  }

  animateRed() {
    this.animateColor(RED);
  }

  animateColor(rgb) {
    window.requestAnimationFrame(this.animationLoop.bind(this, rgb));
  }

  animationLoop(rgb) {
    const currentRGB = this.getRGB();

    console.log('stop?', Color.isCurrentRGB(rgb, currentRGB));

    if (Color.isCurrentRGB(rgb, currentRGB)) return;

    const nextRGB = Color.getNextRGB(rgb, currentRGB);

    this.setRGB(nextRGB);
    this.animateColor(rgb);
  }

  setRGB(rgb) {
    this.element.style.backgroundColor = Color.renderRGB(rgb);
  }

  getRGB() {
    return Color.parseRGB(this.element.style.backgroundColor);
  }

  static renderRGB(rgb) {
    return `rgb(${rgb.join(',')})`;
  }

  static parseRGB(rgbString) {
    const rgbValues = rgbString.match(RGB_STRING_VALUES_REGEX);

    return [
      parseInt(rgbValues[0], 10),
      parseInt(rgbValues[1], 10),
      parseInt(rgbValues[2], 10),
    ];
  }

  static isCurrentRGB(rgb, otherRGB) {
    return rgb.every((value, index) => Color.valuesInBounds(value, otherRGB[index]));
  }

  static getNextRGB(rgb, currentRGB) {
    return currentRGB.map((value, index) => {
      if (value > rgb[index]) return value - RGB_VALUE_INCREMENT;
      if (value < rgb[index]) return value + RGB_VALUE_INCREMENT;
      return value;
    });
  }

  static valuesInBounds(value, otherValue) {
    const min = otherValue - RGB_VALUE_INCREMENT;
    const max = otherValue + RGB_VALUE_INCREMENT;

    return value >= min && value <= max;
  }
}

export default Color;
