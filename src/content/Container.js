import Color from './Color';
import Popup from './Popup';

const CONTAINER_PADDING = 3;

class Container {
  constructor() {
    this.element = document.createElement('div');
    this.element.classList.add('perspective-extension-container');

    this.color = new Color(this.element);
    this.popup = new Popup(this.element);
  }

  hide() {
    this.element.classList.remove('show');
  }

  show() {
    this.element.classList.add('show');
  }

  position(inputElement) {
    const position = this.calculatePosition(inputElement);

    this.element.style.top = `${this.calculateTop(inputElement, position.top)}px`;
    this.element.style.left = `${this.calculateLeft(inputElement, position.left)}px`;
  }

  calculateTop(inputElement, positionTop) {
    let top = positionTop;
    top += inputElement.offsetHeight / 2;
    top -= this.element.offsetHeight / 2;

    return Math.round(top);
  }

  calculateLeft(inputElement, positionLeft) {
    let left = positionLeft;
    left += inputElement.offsetWidth;
    left -= this.element.offsetWidth;
    left -= CONTAINER_PADDING;

    return Math.round(left);
  }

  calculatePosition(element, position = { top: 0, left: 0 }) {
    if (element.tagName === 'BODY') return position;

    position.left += element.offsetLeft;
    position.top += element.offsetTop;

    return this.calculatePosition(element.offsetParent, position);
  }
}

export default Container;
