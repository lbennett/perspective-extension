import Container from './Container';
import Service from './Service';

class Input {
  constructor(element) {
    this.element = element;
  }

  init() {
    this.createContainer();
    this.element.addEventListener('input', this.requestPerspective.bind(this));
  }

  createContainer() {
    this.container = new Container();

    document.body.appendChild(this.container.element);

    this.container.color.setDefaultColor();
    this.container.popup.bindEvents();
    this.container.position(this.element);
    this.container.show();
  }

  requestPerspective(event) {
    const value = event.target.value;

    this.container.color.animateDefaultColor();

    Service.debounceRequest(value, this.renderPerspective.bind(this));
  }

  renderPerspective(error, data) {
    if (error) return this.renderError(error);

    const value = data.attributeScores.TOXICITY.summaryScore.value;
    return this.container.color.setColorFromValue(value);
  }

  renderError(error) {
    if (error.message === 'No text value provided.') return this.container.color.animateDefaultColor();

    this.container.color.animateRed();
    /* eslint-disable no-console */
    console.error('error', error);
    /* eslint-enable no-console */
  }
}

export default Input;
