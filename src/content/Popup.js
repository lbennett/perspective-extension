class Popup {
  constructor(triggerElement) {
    this.triggerElement = triggerElement;

    this.element = document.createElement('div');
    this.element.classList.add('perspective-extension-popup');
  }

  bindEvents() {
    this.triggerElement.addEventListener('mouseenter', this.init.bind(this));
    this.triggerElement.addEventListener('mouseleave', this.hide.bind(this));
  }

  init() {
    document.body.appendChild(this.element);
    this.position();
    this.show();
  }

  position() {
    this.element.style.top = this.triggerElement.style.top;
    this.element.style.left = this.triggerElement.style.left;
  }

  hide() {
    this.element.classList.remove('show');
  }

  show() {
    this.element.classList.add('show');
  }
}

export default Popup;
